#include <parallelus/ParallelUS.hpp>
#include <android/log.h>
#include <stddef.h>
#include "org_parallelme_samples_bitcoinminer_BitcoinMinerNativeCalls.h"
#include <fstream>
#include "kernel_sha256.h"
#include <streambuf>

using namespace parallelus;

struct NativeData {
    std::shared_ptr<Runtime> runtime;
    std::shared_ptr<Program> program;   
};

struct HexCharStruct {
  unsigned char c;
  HexCharStruct(unsigned char _c) : c(_c) { }
};

inline std::ostream& operator<<(std::ostream& o, const HexCharStruct& hs) {
  return (o << std::hex << (int)hs.c);
}

inline HexCharStruct hex(unsigned char _c) {
  return HexCharStruct(_c);
}

std::string HexLitteEndian(std::string in){
	int len = in.length();

	std::string newString;
	for(int i=len-2; i>= 0; i-=2){
    	std::string byte = in.substr(i,2);
    	char chr = (char) (uint32_t)strtol(byte.c_str(), NULL, 16);
    	newString.push_back(chr);
	}

	return newString;
}

std::string pack_uint32_be(uint32_t val) {
    unsigned char packed[4];
    packed[3] = val >> 24;
    packed[2] = val >> 16 & 0xff;
    packed[1] = val >> 8 & 0xff;
    packed[0] = val & 0xff;
    return std::string(packed, packed + 4);
}

void printStringHex(std::string s){

	std::cout << s.size() << std::endl;
	for(int i=0; i < (int)s.size(); i++){
		 __android_log_print(ANDROID_LOG_ERROR, "DEBUG","%x",s[i]);
		//printf("%02x",hex(s[i]));
	}
	std::cout << std::endl;
}

void printCharHex(char* s, int bytes){
	
	for(int i=0; i < bytes; i++){
		__android_log_print(ANDROID_LOG_ERROR, "DEBUG","%02x",s[i]);
	}
	printf("\n");
}

void copyString2Char(std::string s, char **out){
	char* buf_ptr =  *out;
	for(int i=0; i < (int)s.size(); i+=1){
		buf_ptr += sprintf(buf_ptr, "%c", s[i]);
	}
}
JNIEXPORT jlong JNICALL Java_org_parallelme_samples_bitcoinminer_BitcoinMinerNativeCalls_nativeInit
        (JNIEnv *env, jobject self) {

    JavaVM *jvm;
    env->GetJavaVM(&jvm);
    if(!jvm) return (jlong) nullptr;
    auto dataPointer = new NativeData();
    dataPointer->runtime = std::make_shared<Runtime>(jvm,true);
    dataPointer->program = std::make_shared<Program>(dataPointer->runtime, sha256Kernel);

    return (jlong) dataPointer;
}

JNIEXPORT void JNICALL Java_org_parallelme_samples_bitcoinminer_BitcoinMinerNativeCalls_nativeCleanUp
        (JNIEnv *env, jobject self, jlong dataLong) {
    auto dataPointer = (NativeData *) dataLong;
    delete dataPointer;
}
JNIEXPORT void JNICALL Java_org_parallelme_samples_bitcoinminer_BitcoinMinerNativeCalls_nativeBitcoinMiner
        (JNIEnv *env, jobject self, jlong dataLong, jstring header, jobject lock, jintArray aditionalInfo, jbyteArray digest_array, jintArray hashCount_array) {

	//-------------------------- Data info buffer -------------------------------
    jint *infos = env->GetIntArrayElements(aditionalInfo, 0);
    int exec 		= (int)infos[0];
	int target 		= (int)infos[1]; 
	int nonce_base 	= (int)infos[2]; 
	int blockSize 	= (int)infos[3]; 
	env->ReleaseIntArrayElements(aditionalInfo, infos, 0);
    datai[0] = SHA256_PLAINTEXT_LENGTH;
	datai[1] = global_work_size;
	datai[2] = blockSize;

	//-------------------------- ParallelUS Stuffs ------------------------------
	auto dataPointer = (NativeData *) dataLong;
	auto lock1 = env->NewGlobalRef(lock);
	auto digest_array_ref = (jbyteArray) env->NewGlobalRef(digest_array);
	auto hashCount_array_ref = (jintArray) env->NewGlobalRef(hashCount_array);
	
	//-------------------------- Buffers ---------------------------------------
	auto buffer_keys = std::make_shared<Buffer>(Buffer::sizeGenerator(80, Buffer::BYTE));
	auto buffer_out = std::make_shared<Buffer>(Buffer::sizeGenerator(8, Buffer::CHAR4));
	auto data_info = std::make_shared<Buffer>(Buffer::sizeGenerator(3, Buffer::BYTE4));
	auto num_hashs = std::make_shared<Buffer>(Buffer::sizeGenerator(2, Buffer::INT));

	//-------------------------- Buffers Allocation ----------------------------
	char *header_str = (char*) env->GetStringUTFChars(header, 0);
    buffer_keys->setSource(header_str);
    data_info->setSource(datai);
    env->ReleaseStringUTFChars(header, header_str);
 
    //-------------------------- Task ------------------------------------------
 	auto task = std::make_unique<Task>(dataPointer->program);

    task->addKernel("sha256_kernel2");
	task->setConfigFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type) {
		  
	    kernelHash["sha256_kernel2"]
			->setArg(0, data_info, type)
			->setArg(1, buffer_keys, type)
			->setArg(2, buffer_out, type)
			->setArg(3, nonce_base, type)
			->setArg(4, target, type)
			->setArg(5, num_hashs, type)
			->setWorkSize(1,1,1,type);
    });

	task->setFinishFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type){   
            device->JNIEnv()->MonitorEnter(lock1);
   			num_hashs->copyToJArray(device->JNIEnv(),hashCount_array_ref,0);
        	buffer_out->copyToJArray(device->JNIEnv(),digest_array_ref,1);     
    		device->JNIEnv()->MonitorExit(lock1);
    });

    dataPointer->runtime->submitTask(std::move(task), exec);
}

JNIEXPORT void JNICALL Java_org_parallelme_samples_bitcoinminer_BitcoinMinerNativeCalls_nativeWaitFinish
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	dataPointer->runtime->finish();
}
