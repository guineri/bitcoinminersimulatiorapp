package org.parallelme.samples.bitcoinminer;
import android.app.Activity;
import android.os.AsyncTask;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class MainActivity extends Activity {
    private BitcoinMinerOperator operator;

    private TextView mBenchmarkText;
    private EditText mStreamTime;
    private CheckBox mNetCheck;
    private CheckBox mUpsCheck;
    private Boolean networkers;
    private Boolean upworkers;
    long streamTime;

    private BitcoinMinerNativeCalls bitcoinMinerNative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBenchmarkText = (TextView) findViewById(R.id.bench);
        mStreamTime = (EditText) findViewById(R.id.stream_time);
        mNetCheck = (CheckBox) findViewById(R.id.networkers_check);
        mUpsCheck = (CheckBox) findViewById(R.id.ups_check);

        bitcoinMinerNative = new BitcoinMinerNativeCalls();

        upworkers = false;
        networkers = false;

        mStreamTime.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (mStreamTime.getText().toString().compareTo("") != 0) {
                    streamTime = Integer.parseInt(mStreamTime.getText().toString());
                    streamTime = streamTime * 1000; //s to ms
                } else {
                    streamTime = 0;
                }

            }
        });
    }

    public void onCheckBoxClicked(View v){

        boolean checked = ((CheckBox) v).isChecked();

        switch(v.getId()){
            case R.id.networkers_check:
                networkers = checked;
                break;
            case R.id.ups_check:
                upworkers = checked;
                break;
        }
    }

    public void BitcoinMinerStart(View view) throws Exception {
        //mProgressDialog.show();
        new BitcoinMinerSimulationTask().execute(bitcoinMinerNative);
    }

    public void showOriginal(View view) {
        //Nothing
    }

    public void startMiner(View view) {
        new BitcoinMinerSimulationTask().execute(bitcoinMinerNative);
    }


    private class BitcoinMinerSimulationTask extends AsyncTask<BitcoinMinerNativeCalls, Void, String> {
        long mTime;
        long tasksExecuted;

        private final Semaphore available = new Semaphore(1);
        private Object lock = new Object();

        public String bytesToHex(byte[] bytes) {
            //char[] hexChars = new char[65];
            String hexChars = new String("");
            for(int i = 0; i < 8; i++) {
                for (int j = 3; j >= 0; j--) {
                    //System.out.println(String.format("%02x", bytes[j * i]));
                    hexChars += String.format("%02x", bytes[i*4 + j]);
                }
            }
            return hexChars;
        }

        public String charsToHex(char[] bytes) {
            //char[] hexChars = new char[65];
            String hexChars = new String("");
            for(int i = 0; i < 80; i++) {
                    //System.out.println(String.format("%02x", (byte)bytes[i]));
                    hexChars += String.format("%02x", (byte)bytes[i]);
            }
            return hexChars;
        }

        String HexLitteEndian(String in){
            int len = in.length();
            //System.out.println("Tamanho: " + len);
            String newString ="";
            for(int i=len-2; i>= 0; i-=2){
                String bytes = in.substring(i,i+2);
                //System.out.println(bytes + " --");
                char chr = (char) Long.parseLong(bytes, 16);
                newString += chr;
            }
            return newString;
        }

        String pack_uint32_be(long val) {
            char[] packed = new char[4];
            packed[3] = (char) (val >> 24);
            packed[2] = (char) (val >> 16 & 0xff);
            packed[1] = (char) (val >> 8 & 0xff);
            packed[0] = (char) (val & 0xff);
            return new String(packed);
        }

        String randomSHA256(){
            char[] charss = "0123456789abcdef".toCharArray();
            Random rand = new Random();
            String newSha256 = "";
            for(int i=0; i<64; i++){
                newSha256 += charss[rand.nextInt(15) + 0];
            }
            return newSha256;
        }

        String createBlock(String hash_anterior){
            /*int version = 1;
            String hash_anterior = new String("000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f");
            String raiz_markle = new String("0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098");
            long nonce = 00000000;
            long bits = 486604799;
            Date date = new Date();
            long start_time = date.getTime();*/

            int version = 1;
            if(hash_anterior.compareTo("") == 0)
                hash_anterior = randomSHA256();
            String raiz_markle = randomSHA256();
            long nonce = 00000000;
            long bits = 486604799;
            Date date = new Date();
            long start_time = date.getTime();

            String version_hex;			    //4
            String hash_anterior_hex;		//32
            String raiz_markle_hex;		    //32
            String bits_hex;				//4
            String start_time_hex;			//4
            String nonce_hex;				//4
            String header;

            version_hex			= pack_uint32_be(version);
            hash_anterior_hex 	= HexLitteEndian(hash_anterior);
            raiz_markle_hex 	= HexLitteEndian(raiz_markle);
            start_time_hex 		= pack_uint32_be(start_time);
            bits_hex 			= pack_uint32_be(bits);
            nonce_hex 			= pack_uint32_be(nonce);

            header = version_hex
                    + hash_anterior_hex
                    + raiz_markle_hex
                    + start_time_hex
                    + bits_hex
                    + nonce_hex;

            return header;
        }

        Boolean blockProcessed(int hashCount){
            if(hashCount == -1) return false;
            return true;
        }

        @Override
        protected String doInBackground(BitcoinMinerNativeCalls... bitcoinminerOP) {
            long time;

            int numCurrentBlocks = 100;
            int blockSize_default = 80;
            int nonce_base_default = 0;
            int target_default = 1;
            int execution = 2;

            if(upworkers && networkers) execution = 1;          //Scheduled extern and inter
            else if(upworkers && !networkers) execution = 2;    //Just Inter
            else if (!upworkers && networkers) execution = 3;   //Just extern

            Vector<String> blocksIn = new Vector<>();
            Vector<byte[]> blocksOut = new Vector<>();
            Vector<int[]> aditionalInfos = new Vector<>();
            Vector<int[]> hashCount = new Vector<>();
            Vector<Boolean> blockSubmited = new Vector<>();

            for(int i = 0; i < numCurrentBlocks; ++i) {
                    blocksIn.add(createBlock(""));
                    blocksOut.add(new byte[65]);
                    int[] infos = new int[4];
                    infos[0] = execution;
                    infos[1] = target_default;
                    infos[2] = nonce_base_default;
                    infos[3] = blockSize_default;
                    aditionalInfos.add(infos);
                    int[] hc = new int[2];
                    hc[0] = -1;
                    hc[1] = -1;
                    hashCount.add(hc);
                    blockSubmited.add(false);
            }
            int blocksCompleted = 0;
            int currentTarget = 1;
            long totalHashComputed = 0;
            long totalTime = 0;
            mTime = 0;
            tasksExecuted = 0;

            time = java.lang.System.currentTimeMillis();
            totalTime = java.lang.System.currentTimeMillis();

            while(streamTime > mTime) {
                for(int i = 0; i < numCurrentBlocks; ++i) {
                    if (!blockProcessed(hashCount.get(i)[0])) {
                        if (!blockSubmited.get(i)) {
                            bitcoinminerOP[0].bitcoinminer(blocksIn.get(i), lock, aditionalInfos.get(i), blocksOut.get(i), hashCount.get(i));
                            tasksExecuted++;
                            blockSubmited.add(i, true);
                            System.out.println("Submetendo block: " + i);
                        }
                    }
                    else {
                        if(hashCount.get(i)[0] == 0){
                            System.out.println("Block NOT Completed: " + i);
                            // Block not completed, resubmit block with new nonce_base
                            aditionalInfos.get(i)[1] = currentTarget;
                            aditionalInfos.get(i)[2] += 10000000;
                            hashCount.get(i)[0] = -1;
                            hashCount.get(i)[1] = -1;
                            blocksOut.removeElementAt(i);
                            blocksOut.add(i, new byte[65]);

                            bitcoinminerOP[0].bitcoinminer(blocksIn.get(i), lock, aditionalInfos.get(i), blocksOut.get(i), hashCount.get(i));

                            blockSubmited.add(i, true);
                            tasksExecuted++;
                            totalHashComputed += 10000000;
                        }
                       else{
                            System.out.println("Block Completed: " + i);
                            // Block completed! Increment blocksCompleted, update currentTarget!
                            // Submite new block using digest as hash_anterior
                            blocksCompleted += 1;
                            if(blocksCompleted == 1016){
                                currentTarget += 1;
                                blocksCompleted = 0;
                                System.out.println("Target updated: " + currentTarget);
                            }

                            // clear all infos of completed block
                            String newBlock = createBlock(bytesToHex(blocksOut.get(i)));
                            int numberHashComputed = hashCount.get(i)[0];
                            blocksIn.removeElementAt(i);
                            blocksOut.removeElementAt(i);
                            aditionalInfos.removeElementAt(i);
                            hashCount.removeElementAt(i);

                            // Create and Submit new block
                            blocksIn.add(i, newBlock);
                            blocksOut.add(i, new byte[65]);
                            int[] infos = new int[4];
                            infos[0] = execution;
                            infos[1] = currentTarget;
                            infos[2] = nonce_base_default;
                            infos[3] = blockSize_default;
                            aditionalInfos.add(i, infos);
                            int[] hc = new int[2];
                            hc[0] = -1;
                            hc[1] = -1;
                            hashCount.add(i, hc);

                            bitcoinminerOP[0].bitcoinminer(blocksIn.get(i), lock, aditionalInfos.get(i), blocksOut.get(i), hashCount.get(i));

                            blockSubmited.add(i, true);
                            tasksExecuted++;
                            totalHashComputed += numberHashComputed;
                        }
                    }
                }
                mTime = (java.lang.System.currentTimeMillis() - time);
            }
            bitcoinminerOP[0].waitFinish();
            totalTime = (java.lang.System.currentTimeMillis() - totalTime);

           long hashThroughput = (totalHashComputed/1000)/(totalTime/1000);
           long taskThroughput = tasksExecuted/ (totalTime/1000);
            String ret = new String();
            ret =   "===========================\n" +
                    "Total Time: " + totalTime/1000 + "s" + "\n" +
                    "Task Executed: " + tasksExecuted + "\n" +
                    "Blocks Completed: " + blocksCompleted + "\n" +
                    "Hash Throughput: " + hashThroughput + "kh/s" + "\n" +
                    "Task Throughput: " + taskThroughput + "task/s" + "\n" +
                    "===========================\n";
            System.out.println(ret);

            /*String block;
            byte[] digestOut;
            String digest_s = null;
            int[] infos;
            int[] hc;
            long avgTime = 0;
            long avgHashCount = 0;

            for(int i = 0; i < 50; i++){
                block = createBlock("");
                digestOut = new byte[65];
                infos = new int[4];
                infos[0] = execution;
                infos[1] = 1;
                infos[2] = nonce_base_default;
                infos[3] = blockSize_default;
                hc = new int[2];
                hc[0] = -1;
                hc[1] = -1;

                time = java.lang.System.currentTimeMillis();
                bitcoinminerOP[0].bitcoinminer(block, lock, infos, digestOut, hc);
                bitcoinminerOP[0].waitFinish();
                mTime = (java.lang.System.currentTimeMillis() - time);

                digest_s = bytesToHex(digestOut);

                avgTime += mTime;
                avgHashCount += hc[0];
            }

            avgTime = avgTime/50;
            avgHashCount = avgHashCount/50;

            String ret = "Digest: " + digest_s + "\n" +
                         "HashCount(AVG): " + avgHashCount + "\n" +
                         "Tempo(AVG): " + avgTime + "\n" +
                         "Vazão(AVG: " + avgHashCount/avgTime;*/
            return ret;
        }

        @Override
        protected void onPostExecute(String result) {
            mBenchmarkText.setText(result);
            //mProgressDialog.dismiss();
        }

    }
}
