package org.parallelme.samples.bitcoinminer;

import java.util.ArrayList;
import java.util.List;

public interface BitcoinMinerOperator {
    void bitcoinminer(String header, Object lock, int[] aditionalInfo, byte[] digest, int[] hashCount);
    void waitFinish();
}
