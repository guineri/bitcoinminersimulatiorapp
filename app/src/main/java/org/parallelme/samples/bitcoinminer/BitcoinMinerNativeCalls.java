package org.parallelme.samples.bitcoinminer;

import java.util.ArrayList;
import java.util.List;

public class BitcoinMinerNativeCalls implements BitcoinMinerOperator {
    private long dataPointer;

    private native long nativeInit();
    private native void nativeCleanUp(long dataPointer);
    private native void nativeBitcoinMiner(long dataPointer, String header, Object lock, int[] aditionalInfo, byte[] digest, int[] hashCount);
    private native void nativeWaitFinish(long dataPointer);

    @Override

    public void bitcoinminer(String header, Object lock, int[] aditionalInfo, byte[] digest, int[] hashCount) {
        nativeBitcoinMiner(dataPointer, header, lock, aditionalInfo, digest, hashCount);
    }


    public void waitFinish(){
        nativeWaitFinish(dataPointer);
    }

     public boolean inited() {
        return dataPointer != 0;
    }

    BitcoinMinerNativeCalls() {
        dataPointer = nativeInit();
    }

    protected void finalize() throws Throwable {
        try {
            if(dataPointer != 0)
                nativeCleanUp(dataPointer);
        } catch(Throwable t) {
            throw t;
        } finally {
            super.finalize();
        }
    }

    static {
        System.loadLibrary("BitcoinMinerSimulation");
    }
}